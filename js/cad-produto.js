unction validaFormulario(form)
{
	if(form.prdCodBarras.value === " " || form.prdCodBarras.value == null)
	{
		alert("Dados invalidos no campo COD. DE BARRAS");
		form.prdCodBarras.focus();
		return;
	}

	if(form.prodNome.value === " " || form.prodNome.value == null)
	{
		alert("Dados invalidos no campo NOME");
		form.prodNome.focus();
		return;
	}

	if(form.prodDesc.value === " " || form.prodDesc.value == null)
	{
		alert("Dados invalidos no campo DESCRIÇÃO");
		form.prodDesc.focus();
		return;
	}

	if(form.prodDataVenc.value === " " || form.prodDataVenc.value == null || validaData(form.prodDataVenc.value))
	{
		alert("Dados invalidos no campo DATA DE VENCIMENTO");
		form.prodDataVenc.focus();
		return;
	}

	if(form.prdPreco.value === " " || form.prdPreco.value == null)
	{
		alert("Dados invalidos no campo PREÇO");
		form.prdPreco.focus();
		return;
	}

	alert("PRODUTO: "+form.prodNome.value+" CADASTRADO COM SUCESSO !")

}

function validaData(periodo)
{
	var array = periodo.split("/");

	if(array[0]>0 && array[0]<32)
	{
		if(array[1]<0 && array[1]>13)
		{
			return false;
		}

		else
		{
			return true;
		}
	}

	else
	{
		return true;
	}
}